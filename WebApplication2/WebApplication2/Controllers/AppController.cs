﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Data;
using WebApplication2.Services;
using WebApplication2.ViewModels;

namespace WebApplication2.Controllers
{
    public class AppController : Controller
    {
        private readonly IMailService _mailservice;
        private readonly WebContext _ctx;
        private readonly IWebRepository _rep;

        public AppController(IMailService mailservice, WebContext ctx, IWebRepository rep)
        {
            _mailservice = mailservice;
            _ctx = ctx;
            _rep = rep;
        }

        [HttpGet("/")]
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost("/")]
        public IActionResult Index(object model)
        {
            return View();
        }
        [HttpGet("contact")]
        public IActionResult Contact()
        {
            return View();
        }
        [HttpPost("contact")]
        public IActionResult Contact(ContactViewModel model)
        {
            if (ModelState.IsValid) 
            {
                _mailservice.SendMessage("ali123@gmail.com", model.Subject, model.Message);
                ViewBag.UserMessage = "Mail sent";
                ModelState.Clear();
            }
            else 
            {
                System.Diagnostics.Debug.WriteLine(false);
            }

            //System.Diagnostics.Debug.WriteLine(model);
            return View();
        }
        public IActionResult About()
        {
            return View();
        }
        [HttpGet("shop")]
        public IActionResult Shop()
        {
            var results = _ctx.Products
                .OrderBy(p => p.Category)
                .ToList();

            return View(results);
        }
        [HttpPost("shop")]
        public IActionResult Shop(ShopViewModel model)
        {
            /*var results = _ctx.Products
                .OrderBy(p => p.Category)
                .ToList();
*/
            var results = _rep.GetAllProductsByCategory(model.Category);
            //return View(results);
            return View();
        }
    }
}
