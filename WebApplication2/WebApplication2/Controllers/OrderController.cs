﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Data;
using WebApplication2.Data.Entities;
using WebApplication2.ViewModels;

namespace WebApplication2.Controllers
{
    [Route("api/[Controller]")]
    public class OrderController : Controller
    {
        private readonly IWebRepository _repository;
        private readonly ILogger<OrderController> _log;
        private readonly IMapper _mapper;

        public OrderController(IWebRepository repository, ILogger<OrderController> log, IMapper mapper)
        {
            _repository = repository;
            _log = log;
            _mapper = mapper;
        }
        [HttpGet]
        public IActionResult Get() 
        {
            try
            {
                _log.LogInformation("Getting all Orders");
                return Ok(_repository.GetAllOrders());
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get orders: {ex}");
                return BadRequest("Failed to get orders");
            }
        }
        [HttpGet("{id:int}")]
        public IActionResult Get(int id)
        {
            try
            {
                _log.LogInformation("Getting all Orders");
                return Ok(_repository.GetOrdersbyId(id));
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get orders: {ex}");
                return BadRequest("Failed to get orders");
            }
        }
        [HttpPost]
        public IActionResult Post([FromBody]OrderViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    /*var newOrder = new Order()
                    {
                        OrderDate = model.OrderDate,
                        OrderNumber = model.OrderNumber,
                        Id = model.OrderID
                    };
                    */
                    var newOrder = _mapper.Map<OrderViewModel, Order>(model);
                    if (newOrder.OrderDate == DateTime.MinValue) 
                    {
                        newOrder.OrderDate = DateTime.Now;
                    }
                    _repository.AddEntities(newOrder);
                    if (_repository.SaveAll())  
                    {
                        /*var vm = new OrderViewModel()
                        {
                            OrderID = newOrder.Id,
                            OrderDate = newOrder.OrderDate,
                            OrderNumber = newOrder.OrderNumber
                        };
                        */
                        var vm = _mapper.Map<Order, OrderViewModel>(newOrder);
                        return Created($"/api/order/{vm.OrderID}", model);
                    }
                }
                else 
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get orders: {ex}");
            }
            return BadRequest("Failed to get orders");
        }
    }
}
