﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Data;
using WebApplication2.Data.Entities;
using WebApplication2.ViewModels;

namespace WebApplication2.Controllers
{
    [Route("/api/orders/{orderid}/items")]
    public class OrderItemController : Controller
    {
        private readonly IWebRepository _repository;
        private readonly ILogger _log;
        private readonly IMapper _mapper;
        public OrderItemController(IWebRepository repository, ILogger<OrderItemController> log, IMapper mapper)
        {
            _repository = repository;
            _log = log;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Get(int orderId) 
        {
            //IEnumerable<Order> order = _repository.GetOrdersbyId(orderId);
            System.Diagnostics.Debug.WriteLine(order.ToList()[0].Items.ToString());
            var order = _repository.GetOrdersbyId(orderId);
            /*foreach (Order item in order)
            {
                System.Diagnostics.Debug.WriteLine(item);
                item.Items;
            }*/
            if (order != null)
            {
                //return Ok();
                return Ok(_mapper.Map<IEnumerable<Order>, IEnumerable<OrderItemViewModel>>(order.Items));
            }
            return Ok();
        }
    }
}
