﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Data;

namespace WebApplication2.Controllers
{
    [Route("api/[Controller]")]
    public class ProductController : Controller
    {
        private readonly IWebRepository _repository;
        private readonly ILogger<ProductController> _log;

        public ProductController(IWebRepository repository, ILogger<ProductController> log)
        {
            _repository = repository;
            _log = log;
        }
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                _log.LogInformation("Getting all products");
                return Ok(_repository.GetAllProducts());
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get product: {ex}");
                return BadRequest("Failed to get products");
            }
        }
    }
}
