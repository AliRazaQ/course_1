﻿using System.Collections.Generic;
using WebApplication2.Data.Entities;

namespace WebApplication2.Data
{
    public interface IWebRepository
    {
        IEnumerable<Product> GetAllProducts();
        IEnumerable<Product> GetAllProductsByCategory(string Category);
        bool SaveAll();
        IEnumerable<Order> GetAllOrders();
        IEnumerable<Order> GetOrdersbyId(int id);
        void AddEntities(object model);
    }
}