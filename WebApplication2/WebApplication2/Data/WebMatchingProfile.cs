﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Data.Entities;
using WebApplication2.ViewModels;

namespace WebApplication2.Data
{
    public class WebMatchingProfile : Profile
    {
        public WebMatchingProfile()
        {
            CreateMap<OrderViewModel, Order>()
                   .ForMember(o => o.Id, ex => ex.MapFrom(o => o.OrderID))
                   .ReverseMap();
        }
    }
}
