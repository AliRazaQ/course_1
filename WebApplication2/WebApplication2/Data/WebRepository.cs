﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Data.Entities;

namespace WebApplication2.Data
{
    public class WebRepository : IWebRepository
    {
        private readonly WebContext _ctx;
        private readonly ILogger<WebRepository> _log;

        public WebRepository(WebContext ctx, ILogger<WebRepository> log)
        {
            _ctx = ctx;
            _log = log;
        }

        public void AddEntities(object model)
        {
            _ctx.Add(model);
            //_ctx.SaveChanges();
        }

        public IEnumerable<Order> GetAllOrders()
        {
            try
            {
                _log.LogInformation("Getting all Orders");
                return _ctx.Orders
                           .Include(o => o.Items)
                           .ThenInclude(i => i.Product)
                           .ToList();
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get order: {ex}");
                return null;
            }
        }

        public IEnumerable<Product> GetAllProducts()
        {
            try
            {
                _log.LogInformation("Getting all Products");
                return _ctx.Products
                           .OrderBy(p => p.Title)
                           .ToList();
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get product: {ex}");
                return null;
            }
            
        }

        public IEnumerable<Product> GetAllProductsByCategory(string Category)
        {
            return _ctx.Products
                       .Where(p => p.Title == Category)
                       .OrderBy(p => p.Title)
                       .ToList();
        }

        public IEnumerable<Order> GetOrdersbyId(int id)
        {
            try
            {
                _log.LogInformation($"Getting Orders By Id: {id}");
                return _ctx.Orders
                           .Include(o => o.Items)
                           .ThenInclude(i => i.Product)
                           .Where(i => i.Id == id);
            }
            catch (Exception ex)
            {
                _log.LogError($"Failed to get order: {ex}");
                return null;
            }

        }

        public bool SaveAll()
        {
            return _ctx.SaveChanges() > 0;
        }
    }
}
