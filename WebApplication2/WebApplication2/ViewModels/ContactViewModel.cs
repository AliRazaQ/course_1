﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.ViewModels
{
    public class ContactViewModel
    {
        [Required]
        [MinLength(3, ErrorMessage ="Invalid Name")]
        public string Name { get; set; }
        
        [Required] 
        [EmailAddress(ErrorMessage ="Invalid Email")]
        public string Email { get; set; }
        
        [Required]
        [MaxLength(100, ErrorMessage = "Too Long")]
        public string Subject { get; set; }
        
        [Required]
        [MaxLength(250, ErrorMessage ="Too Long")]
        public string Message { get; set; }
    }
}